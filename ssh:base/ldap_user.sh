#!/bin/bash
#@marc.porto.salvatierra
# -----------------------

llistaUsers="$(getent passwd | grep "*" | cut -d: -f1)"
for user in $llistaUsers
do
  linia=$(getent passwd $user)
  uid=$(echo $linia | cut -d: -f3)
  gid=$(echo $linia | cut -d: -f4)
  homedir=$(echo $linia | cut -d: -f6)
  echo "$user $uid $gid $homedir"
  if [ ! -d $homedir ]; then
    mkdir -p $homedir
    cp -ra /etc/skel/. $homedir
    chown -R $uid.$gid $homedir
  fi
done
