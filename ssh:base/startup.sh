#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/common-session /etc/pam.d/common-session

chmod 777 /opt/docker/ldap_user.sh
bash /opt/docker/ldap_user.sh

/usr/sbin/nscd
/usr/sbin/nslcd
cp /opt/docker/sshd_config /etc/ssh/sshd_config
mkdir /run/sshd
/usr/sbin/sshd -D
