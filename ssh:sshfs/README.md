# PAM

#### PAM

1. Crear la imatge edtasixm06/ssh22:base amb els requisits següents:
- Basada en pam22:ldap de manera que permeti l’autenticació d’usuaris unix
locals i usuaris LDAP.
- Els containers han de poder executar-se en detach.
- Generar el README pertinent i pujar-ho tot al git i a Dockerhub.
2. Aprofitar per entendre els conceptes:
- Procés d’instal·lació, claus de host, tipus de claus.
- Concepte de fingerprint, autenticació host destí.
- Configuració SSH client
- Configuració SSH servidor.
3. Configuració client.
- Practicar configuracions client diverses.
- Documentar les directives de client utilitzades.
4. Configuració servidor.
- Practicar configuracions de servidor diverses.
- Documentar les directives de servidor utilitzades.
5. Accés SSH per clau pública
- Configurar accés SSH destès per clau pública.
Per tal 
- Utilitzar SSH agent localment.
- Documentar el funcionament de l’accés SSH per clau pública.

docker run --rm --name sshfs.edt.org --hostname sshfs.edt.org --network 2hisx --privileged --cap-add SYS_ADMIN --device /dev/fuse  --security-opt apparmor:unconfined -it mporto20/ssh22:sshfs /bin/bash

root@sshfs# ./startup.sh -> dins de sshfs.edt.org

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap22:latest

docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -d mporto20/ssh22:base

a201532mp@g18:~/m06/ssh/ssh:base$ ssh unix01@172.18.0.2

The authenticity of host '172.18.0.2 (172.18.0.2)' can't be established.
ECDSA key fingerprint is SHA256:izmBiCPfZevYUlWauL3etWJ1VcYjqmsBqN7UZgg1iM0.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes

unix01@ssh:~$ 



